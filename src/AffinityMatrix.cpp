/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada and Hideaki Uchiyama.

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   AffinityMatrix.cpp
@brief  to compute affinity matrix for TRDM.
@author Yuji Oyamada and Hideaki Uchiyama
*/

#include "AffinityMatrix.h"

template <typename T>
T distance(
    const cv::Point_<T>& p0,
    const cv::Point_<T>& p1
)
{
    return static_cast<T>( cv::norm(p0-p1) );
}

double distanceFeature(
    const cv::Mat& p0,
    const cv::Mat& p1
)
{
    double val = p0.dot(p1) / (cv::norm(p0)*cv::norm(p1));
    if(val<0.0)
    {
        val = 0.0;
    }
    else if(val>1.0)
    {
        val = 1.0;
    }
    return val;
}

template <typename T>
T computeDistanceRatio(
    const T dist0,
    const T dist1
)
{
    T eps = std::numeric_limits<T>::epsilon();
    if(dist0>dist1)
    {
        return (dist1+eps)/(dist0+eps);
    }
    else
    {
        return (dist0+eps)/(dist1+eps);
    }
}

Affinity::Affinity():
    distThreshold(0.8f),
    methodAffinity(0),
    countNonZero(0)
{

}
Affinity::~Affinity()
{

}

void Affinity::init(
    const std::vector< std::vector<cv::Point2f> >& pointsReference,
    const std::vector<cv::Point2f>& pointsQuery,
    Eigen::SparseMatrix<float>& matAffinity
)
{
    numPointsQuery = int(pointsQuery.size());
    numPointsetsReference = int(pointsReference.size());
    numPointsReference = std::vector<int>(numPointsetsReference, 0);
    numCorrespondences = std::vector<int>(numPointsetsReference, 0);
    startIndex = std::vector<int>(numPointsetsReference, 0);
    countNonZero = 0;
    for(int n = 0; n < numPointsetsReference; ++n)
    {
        numPointsReference[n] = int(pointsReference[n].size());
        numCorrespondences[n] = int(pointsReference[n].size())*numPointsQuery;
        if(n != 0)
        {
            startIndex[n] = startIndex[n-1] + numCorrespondences[n-1];
        }
    }

    assert(
        matAffinity.rows() == matAffinity.cols() &&
        matAffinity.rows() == startIndex[numPointsetsReference-1]+numCorrespondences[numPointsetsReference-1] &&
        "The size of affinity matrix must be correct."
    );

    int size = matAffinity.rows();
    tripletList = std::vector< Eigen::Triplet<float> >(size*size/numPointsetsReference);
}

void Affinity::init(
    const std::vector<cv::Point2f>& pointsReference,
    const std::vector<cv::Point2f>& pointsQuery,
    Eigen::SparseMatrix<float>& matAffinity
)
{
    numPointsQuery = int(pointsQuery.size());
    numPointsetsReference = 1;
    numPointsReference = std::vector<int>(numPointsetsReference, int(pointsReference.size()));
    numCorrespondences = std::vector<int>(numPointsetsReference, int(pointsReference.size())*numPointsQuery);
    startIndex = std::vector<int>(numPointsetsReference, 0);
    countNonZero = 0;

    assert(
        matAffinity.rows() == matAffinity.cols() &&
        matAffinity.rows() == numCorrespondences[0] &&
        "The size of affinity matrix must be correct."
    );

    int size = matAffinity.rows();
    tripletList = std::vector< Eigen::Triplet<float> >(size*size/numPointsetsReference);
}

void Affinity::set(
    Eigen::SparseMatrix<float>& matAffinity
) const
{
    matAffinity.setFromTriplets(tripletList.begin(), tripletList.begin()+countNonZero);
}

void Affinity::computeAffinity(
    const std::vector< std::vector<cv::Point2f> >& pointsReference,
    const std::vector<cv::Point2f>& pointsQuery,
    Eigen::SparseMatrix<float>& matAffinity
)
{
    init(
        pointsReference,
        pointsQuery,
        matAffinity
    );
    addPairwiseTerm(
        pointsReference,
        pointsQuery
    );
    set(matAffinity);
}

void Affinity::computeAffinity(
    const std::vector<cv::Point2f>& pointsReference,
    const std::vector<cv::Point2f>& pointsQuery,
    Eigen::SparseMatrix<float>& matAffinity
)
{
    init(
        pointsReference,
        pointsQuery,
        matAffinity
    );
    addPairwiseTerm(
        pointsReference,
        pointsQuery
    );
    set(matAffinity);
}

void Affinity::computeAffinity(
    const std::vector< std::vector<cv::Point2f> >& pointsReference,
    const std::vector<cv::Point2f>& pointsQuery,
    const std::vector< std::vector<cv::Mat> >& featuresReference,
    const std::vector<cv::Mat>& featuresQuery,
    Eigen::SparseMatrix<float>& matAffinity
)
{
    init(
        pointsReference,
        pointsQuery,
        matAffinity
    );
    addPairwiseTerm(
        pointsReference,
        pointsQuery
    );
    addUnaryTerm(
        featuresReference,
        featuresQuery
    );
    set(matAffinity);
}

void getMarkerIndex(
    const int& n,
    const std::vector<int>& startIndex,
    int& mid
)
{
    for(int m = 0; m < startIndex.size(); ++m)
    {
        if(n < startIndex[m])
        {
            mid = m;
            break;
        }
    }
}

void Affinity::addPairwiseTerm(
    const std::vector< std::vector<cv::Point2f> >& pointsReference,
    const std::vector<cv::Point2f>& pointsQuery
)
{
    assert(
        pointsReference.size() > 0 &&
        pointsQuery.size() > 0 &&
        "The size of the pointsets must be greather than 0."
    );

    // compare two feature vectors iFeature and jFeature such that
    // iFeature = feature vectorr defined by reference points p[r][i0] and p[r][i1]
    // jFeature = feature vectorr defined by query points q[j0] and q[j1]
    float iFeature, jFeature, affinity;
    int r, i0, i1, j0, j1, n0, n1;

    for(r = 0; r < numPointsetsReference; ++r)
    { // pairwise term of r-th reference pointset
        for(i0 = 0; i0 < numPointsReference[r]; ++i0)
        {
            for(j0 = 0; j0 < numPointsQuery; ++j0)
            {
                for(i1 = i0+1; i1 < numPointsReference[r]; ++i1)
                {
                    for(j1 = 0; j1 < numPointsQuery; ++j1)
                    {
                        if(j0 != j1)
                        {
                            // set row
                            n0 = i0*numPointsQuery+j0 + startIndex[r];
                            iFeature = distance(pointsReference[r][i0], pointsReference[r][i1]);
                            n1 = i1*numPointsQuery+j1 + startIndex[r];
                            jFeature = distance(pointsQuery[j0], pointsQuery[j1]);
                            affinity = computeDistanceRatio(iFeature, jFeature);
                            if(affinity > distThreshold)
                            {
                                tripletList[++countNonZero] = Eigen::Triplet<float>(n0, n1, affinity);
                                tripletList[++countNonZero] = Eigen::Triplet<float>(n1, n0, affinity);
                            }
                        }
                    }
                }
            }
        }
    }
}

void Affinity::addPairwiseTerm(
    const std::vector<cv::Point2f>& pointsReference,
    const std::vector<cv::Point2f>& pointsQuery
)
{
    assert(
        pointsReference.size() > 0 &&
        pointsQuery.size() > 0 &&
        "The size of the pointsets must be greather than 0."
    );

    // compare two feature vectors iFeature and jFeature such that
    // iFeature = feature vectorr defined by reference points p[r][i0] and p[r][i1]
    // jFeature = feature vectorr defined by query points q[j0] and q[j1]
    float iFeature, jFeature, affinity;
    int i0, i1, j0, j1, n0, n1;

    for(i0 = 0; i0 < numPointsReference[0]; ++i0)
    {
        for(j0 = 0; j0 < numPointsQuery; ++j0)
        {
            n0 = i0*numPointsQuery+j0;
            for(i1 = i0+1; i1 < numPointsReference[0]; ++i1)
            {
                iFeature = distance(pointsReference[i0], pointsReference[i1]);
                for(j1 = 0; j1 < numPointsQuery; ++j1)
                {
                    if(j0 != j1)
                    {
                        // set row
                        n1 = i1*numPointsQuery+j1;
                        jFeature = distance(pointsQuery[j0], pointsQuery[j1]);
                        affinity = computeDistanceRatio(iFeature, jFeature);
                        if(affinity > distThreshold)
                        {
                            tripletList[++countNonZero] = Eigen::Triplet<float>(n0, n1, affinity);
                            tripletList[++countNonZero] = Eigen::Triplet<float>(n1, n0, affinity);
                        }
                    }
                }
            }
        }
    }
}

void Affinity::addUnaryTerm(
    const std::vector< std::vector<cv::Mat> >& featuresReference,
    const std::vector<cv::Mat>& featuresQuery
)
{
    assert(
        featuresReference.size() > 0 &&
        featuresQuery.size() > 0 &&
        "The size of the pointsets must be greather than 0."
    );

    cv::Mat feature0, feature1;
    float affinity;
    int i, j, n;
    // add unary term
    for(int r = 0; r < numPointsetsReference; ++r)
    {
        for(i = 0; i < numPointsReference[r]; ++i)
        {
          feature0 = featuresReference[r][i];
            for(j = 0; j < numPointsQuery; ++j)
            {
                feature1 = featuresQuery[j];
                n = i*numPointsQuery+j + startIndex[r];
                affinity = static_cast<float>( distanceFeature(feature0, feature1) );
                tripletList[++countNonZero] = Eigen::Triplet<float>(n, n, affinity);
            }
        }
    }
}

