/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   CalibrationUtil.h
@brief  utility functions for demoCalibration.cpp
@author Yuji Oyamada
*/

#include <vector>
#include <string>

#include <opencv2/opencv.hpp>

#include "Calibration.h"
#include "LLAH.h"

void decompose_filename(const std::string filename,
                        std::string& dirname,
                        std::string& basename);

int extract_number(const std::string str);

std::vector< cv::Point2f > detect_dots(cv::Mat& img,
                                       const int th_min,
                                       const int th_max);

void draw_points(cv::Mat& img,
                 const std::vector<cv::Point2f>& pts,
                 const std::vector<sRetrieval>& matches);

bool load_matches(const std::string filename,
    std::vector< cv::Point3f >& point_objects,
    std::vector< cv::Point2f >& point_images);

bool load_calibration_data(
    const std::string filename,
    std::vector< std::vector<cv::Point3f> >& objects,
    std::vector< std::vector<cv::Point2f> >& images,
    cv::Size& size_image
);

bool load_calibration_config(
    const std::string filename,
    std::string& filename_markers,
    std::vector<std::string>& filename_images);

void decide_threshold(CLLAH& db, const std::string filename);

bool save_corresponding_points(CLLAH& db,
                               const int th_min,
                               const int th_max,
                               std::vector<std::string>& filename_images,
                               std::string filename_data);

void draw_results(calibration::sParameter& param,
                  std::vector<calibration::points3d>& objects,
                  std::vector<calibration::points2d>& images,
                  std::vector<std::string>& filename_images);
