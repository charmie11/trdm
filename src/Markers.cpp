/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   Markers.cpp
@brief  to generate TRDM markers.
@author Yuji Oyamada
*/

#include <cassert>
#include <random>
#include <limits>


#include "Markers.h"

bool load_config_genmarker(
    const std::string& filename,
    paramMarkers& param
)
{
    cv::FileStorage fs(filename, cv::FileStorage::READ);
    if (!fs.isOpened()){
        return false;
    }

    param.num_markers = fs["num_markers"];
    param.image_width = fs["image_width"];
    param.image_height = fs["image_height"];
    param.num_points = fs["num_points"];

    fs.release();
    return true;
}

void print_markers(const std::vector < std::vector< cv::Point2f > >& markers)
{
    std::cout << "print all markers" << std::endl;
    for(auto marker = markers.cbegin(); marker != markers.cend(); ++marker)
    {
        std::cout << "Marker[" << std::distance(markers.cbegin(), marker) << "] has " << marker->size() << " points" << std::endl;
        for(auto point = marker->cbegin(); point != marker->cend(); ++point)
        {
            std::cout << "    Point[" << std::distance(marker->cbegin(), point) << "] = ";
            std::cout << "(" << point->x << ", " << point->y << ")" << std::endl;
        }
    }
}

std::vector< std::vector< cv::Point2f > > generate_markers(const paramMarkers param)
{
    /////////////////////////
    // settings
    const int min_size = std::min(param.image_width, param.image_height);
    const int pad_size = min_size / 10;
    const int area_size = min_size - 2 * pad_size;
    const int radius = area_size / 50;	// dot radius
    const int interval = radius * 10;		// between two dot centers

    /////////////////////////
    // generate markers
    std::random_device rd;
    std::mt19937 gen(rd());
    std::uniform_real_distribution<> dis(pad_size, pad_size + area_size);

    std::vector< std::vector< cv::Point2f > > markers(param.num_markers);
    for(int n = 0; n < param.num_markers; ++n)
    {
        std::vector< cv::Point2f > points(param.num_points);
        for(int m = 0; m < param.num_points; ++m)
        {
            points[m] = cv::Point2f(float(dis(gen)), float(dis(gen)));
            for(int l = 0; l < m; ++l)
            {
                if(cv::norm(points[m] - points[l]) < interval)
                {
                    --m;
                    break;
                }
            }
        }
        markers[n] = points;
    }

    return markers;
}

bool save_markers_as_xml(
    const std::vector< std::vector< cv::Point2f > >& markers,
    const paramMarkers param,
    const std::string filename
)
{
    cv::FileStorage fs(filename, cv::FileStorage::WRITE);

    fs << "num_markers" << param.num_markers;
    fs << "image_width" << param.image_width;
    fs << "image_height" << param.image_height;
    fs << "num_points" << param.num_points;
    fs << "markers" << "[";
    for(auto marker = markers.cbegin(); marker != markers.cend(); ++marker)
    {
        fs << "[:";
        for(auto point = marker->cbegin(); point != marker->cend(); ++point)
        {
            fs << *point;
        }
        fs << "]";
    }
    fs << "]";
    fs.release();

    return true;
}

bool load_markers_from_xml(
    const std::string filename,
    std::vector< std::vector< cv::Point2f > >& markers,
    paramMarkers& param
)
{
    cv::FileStorage fs(filename, cv::FileStorage::READ);
    if(!fs.isOpened()){
        std::cerr << "Load error of " << filename << std::endl;
        return false;
    }

    param.num_markers = int(fs["num_markers"]);
    param.image_width = int(fs["image_width"]);
    param.image_height = int(fs["image_height"]);
    param.num_points = int(fs["num_points"]);
    std::cout << "#markers: " << param.num_markers << std::endl;
    std::cout << "image width: " << param.image_width << std::endl;
    std::cout << "image height: " << param.image_height << std::endl;
    std::cout << "#points: " << param.num_points << std::endl;

    auto fn = fs["markers"];
    for(auto it_m = fn.begin(); it_m != fn.end(); ++it_m)
    {
        cv::Point2f p;
        std::vector<cv::Point2f> marker;
        for(auto it_p = (*it_m).begin(); it_p != (*it_m).end(); ++it_p)
        {
            *it_p >> p;
            marker.push_back(p);
        }
        markers.push_back(marker);
    }

    return true;
}

bool test_save_load(
    const std::vector< std::vector< cv::Point2f > >& markers,
    const paramMarkers param
)
{
    std::string filename_xml = "../data/tmp_markers.xml";

    // save
    save_markers_as_xml(markers, param, filename_xml);

    // load
    std::vector< std::vector< cv::Point2f > > markers2;
    paramMarkers param2;
    load_markers_from_xml(filename_xml, markers2, param2);

    // validate
    bool flag = true;
    if(param.num_markers != param2.num_markers ||
       param.image_width != param2.image_width ||
       param.image_height != param2.image_height ||
       param.num_points != param2.num_points)
    {
        flag = false;
    }

    auto eps = std::numeric_limits<float>::epsilon();
    for(int n = 0; n < param.num_markers; ++n)
    {
        for(int m = 0; m < param.num_points; ++m)
        {
            if(cv::norm(markers[n][m] - markers2[n][m]) > eps)
            {
                std::cout << n << "," << m << std::endl;
                flag = false;
            }
        }
    }

    std::cout << "save/load passed!" << std::endl;

    return flag;
}

void save_markers_as_image(
    const std::vector< std::vector< cv::Point2f > >& markers,
    const paramMarkers param,
    const std::string dirname,
    const std::string prefix
)
{
    assert(markers.size() == param.num_markers);
    int radius = std::min(param.image_width, param.image_height) / 50;
    for(int n = 0; n < param.num_markers; ++n)
    {
        cv::Mat img = 255*cv::Mat::ones(param.image_width, param.image_height, CV_8UC1);
        for(int m = 0; m < param.num_points; ++m)
        {
            cv::circle(img, markers[n][m], radius, cv::Scalar(0), -1);
        }
        std::string filename = dirname + prefix + std::to_string(n) + ".png";
        cv::imwrite(filename, img);
    }
}
