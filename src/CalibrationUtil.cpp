/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   CalibrationUtil.cpp
@brief  utility functions for demoCalibration.cpp
@author Yuji Oyamada
*/

#include <fstream>
#include <iostream>
#include <sstream>
#include <regex>

#include <opencv2/opencv.hpp>
#include "Markers.h"
#include "CalibrationUtil.h"

void decompose_filename(const std::string filename,
                        std::string& dirname,
                        std::string& basename)
{
    std::stringstream ss{filename};
    std::string buf;
    std::vector< std::string > splits;
    while (std::getline(ss, buf, '/')) {
        splits.push_back(buf);
    }

    int n = 0;
    dirname = "/";
    for(n = 0; n < splits.size()-1; ++n)
    {
        dirname += splits[n] + "/";
    }
    basename = splits[n];
}

int extract_number(const std::string str)
{
    std::smatch match;
    std::regex_search(str,
                      match,
                      std::regex(R"(\d+)"));

    return std::stoi(match.str());
}

std::vector< cv::Point2f > detect_dots(cv::Mat& img,
                                       const int th_min,
                                       const int th_max)
{
    cv::Mat img_edge;
    const int thlow = 30;        // lowest responce for edge
    const int thhigh = 100;        // edge if more than thhigh
    cv::Canny(img, img_edge, thlow, thhigh);

    std::vector< std::vector< cv::Point > > pt_contours;
    cv::findContours(img_edge, pt_contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_NONE);

    std::vector< cv::Point2f > pt_dots;

    // circle size
    for(int n = 0; n < pt_contours.size(); ++n)
    {
        std::vector< cv::Point > &pt_contour = pt_contours[n];
        int size_contour = int(pt_contour.size());
        if(size_contour > th_min && size_contour < th_max)
        {
            cv::RotatedRect rrt = cv::fitEllipse(pt_contour);

            float ratio = rrt.size.width > rrt.size.height ? rrt.size.width / rrt.size.height : rrt.size.height / rrt.size.width;

            const float th = 3.0f;
            if (ratio < th){
                pt_dots.push_back(rrt.center);
            }
        }
    }
    return pt_dots;

}

void draw_points(
                 cv::Mat& img,
                 const std::vector<cv::Point2f>& pts,
                 const std::vector<sRetrieval>& matches
                 )
{
    for(int n = 0; n < pts.size(); ++n)
    {
        cv::circle(img, pts[n], 5, cv::Scalar(0, 0, 32), -1);
    }
    for(int n = 0; n < matches.size(); ++n)
    {
        cv::circle(img, pts[matches[n].id_query], 5, cv::Scalar(0, 0, 255), -1);
    }
}

void draw_markers(
                  cv::Mat& img,
                  const std::vector< sMarker >& markers,
                  const std::vector<cv::Point2f>& pts
                  )
{
    for(int n = 0; n < markers.size(); ++n)
    {
        std::vector<cv::Point2f> pts_img{4};
        cv::perspectiveTransform(pts, pts_img, markers[n].H);
        cv::Point2f pt_center(0.0f, 0.0f);
        for(int m = 0; m < 4; ++m)
        {
            cv::line(img, pts_img[m], pts_img[(m+1)%4], cv::Scalar(255, 0, 0), 3);
            pt_center += pts_img[m] / 4.0f;
        }
        cv::putText(img, std::to_string(markers[n].id), pt_center, 1, 3, cv::Scalar(0, 0, 255));
    }
}

bool load_matches(
    const std::string filename,
    std::vector< cv::Point3f >& point_objects,
    std::vector< cv::Point2f >& point_images
)
{
    cv::FileStorage fs(filename, cv::FileStorage::READ);
    if (!fs.isOpened()){
        std::cerr << "load " << filename << " failed..." << std::endl;
        return false;
    }

    // image information
    cv::FileNode nodes = fs["matches"];
    point_objects.clear();
    point_images.clear();

    cv::Point3f p3(0.0f, 0.0f, 0.0f);
    cv::Point2f p2(0.0f, 0.0f);
    for(auto it = nodes.begin(); it != nodes.end(); ++it)
    {
        (*it)["reference_x"] >> p3.x;
        (*it)["reference_y"] >> p3.y;
        point_objects.push_back(p3);

        (*it)["query_x"] >> p2.x;
        (*it)["query_y"] >> p2.y;
        point_images.push_back(p2);
    }

    fs.release();

    return true;
}

bool load_calibration_data(
    const std::string filename,
    std::vector< std::vector<cv::Point3f> >& objects,
    std::vector< std::vector<cv::Point2f> >& images,
    cv::Size& size_image
)
{
    cv::FileStorage fs(filename, cv::FileStorage::READ);
    if (!fs.isOpened()){
        std::cerr << "load " << filename << " failed..." << std::endl;
        return false;
    }

    // image information
    fs["width"] >> size_image.width;
    fs["height"] >> size_image.height;

    // matching information
    cv::FileNode nodes = fs["matches"];
    objects.clear();
    images.clear();

    std::vector<cv::Point3f> _objects;
    std::vector<cv::Point2f> _images;
    for(auto it = nodes.begin(); it != nodes.end(); ++it)
    {
        std::string tmp;
        *it >> tmp;
        std::cout << "  load " << tmp << std::endl;
        if(!load_matches(tmp, _objects, _images))
        {
            std::cerr << "load " << tmp << " failed..." << std::endl;
            return false;
        }
        objects.push_back(_objects);
        images.push_back(_images);
    }

    fs.release();

    return true;
}

bool load_calibration_config(
    const std::string filename,
    std::string& filename_markers,
    std::vector<std::string>& filename_images)
{
    cv::FileStorage fs(filename, cv::FileStorage::READ);
    if (!fs.isOpened()){
        std::cerr << "load " << filename << " failed..." << std::endl;
        return false;
    }

    // marker information
    filename_markers = (std::string)fs["markers"];

    // image information
    cv::FileNode nodes = fs["images"];
    filename_images.clear();
    for(auto it = nodes.begin(); it != nodes.end(); ++it)
    {
        std::string tmp;
        *it >> tmp;
        filename_images.push_back(tmp);
    }

    fs.release();

    return true;
}

bool save_corresponding_points(CLLAH& db,
                               const int th_min,
                               const int th_max,
                               std::vector<std::string>& filename_images,
                               std::string filename_data){
    cv::FileStorage fs(filename_data, cv::FileStorage::WRITE);
    if (!fs.isOpened()){
        std::cerr << "open " << filename_data << " failed..." << std::endl;
        return false;
    }

    std::string dirname, basename, filename_match;
    cv::Mat _img, img;
    std::vector<sRetrieval> matches;
    for(auto it = filename_images.cbegin(); it != filename_images.cend(); ++it)
    {
        decompose_filename(*it, dirname, basename);
        int id_save = extract_number(basename);

        // detect dots from image
        _img = cv::imread(*it);
        cv::cvtColor(_img, img, cv::COLOR_BGR2GRAY);
        auto pt_queries = detect_dots(img, th_min, th_max);

        // run matching with database
        filename_match = "../data/matches" + std::to_string(id_save) + ".xml";
        db.find(pt_queries, matches);
        db.save_matches(filename_match);

        // save info into xml
        if(it == filename_images.cbegin())
        {
            fs << "width" << _img.size().width;
            fs << "height" << _img.size().height;
            fs << "matches" << "[:";
        }
        fs << filename_match;
    }
    fs << "]";
    fs.release();

    return true;
}

void draw_results(calibration::sParameter& param,
                  std::vector<calibration::points3d>& objects,
                  std::vector<calibration::points2d>& images,
                  std::vector<std::string>& filename_images){

    std::string window_name = "Reprojection error";
    cv::namedWindow(window_name, cv::WINDOW_NORMAL);

    calibration::points2d points;

    for(size_t i = 0; i < filename_images.size(); ++i){
        cv::Mat img = cv::imread(filename_images[i], 1);

        cv::projectPoints(objects[i], param.rvecs[i], param.tvecs[i], param.K, param.delta, points);
        for(size_t j = 0; j < points.size(); ++j){
            cv::circle(img, points[j], 3, cv::Scalar(255, 0, 0), -1);
            cv::rectangle(img, images[i][j]-cv::Point2f(2.0, 2.0), images[i][j]+cv::Point2f(2.0, 2.0), cv::Scalar(0, 0, 255));
        }
        cv::imshow(window_name, img);
        cv::waitKey(0.5);
    }
    cv::destroyAllWindows();

}
