/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   SpectralMethod.cpp
@brief  the core implementation of spectral method.
@author Yuji Oyamada
*/


#include "SpectralMethod.h"
#ifdef _OPENMP
#include <omp.h>
#endif

template <typename T>
T square(
    const T x
)
{
    return x*x;
}

float distance(
    const Eigen::VectorXf& p0,
    const Eigen::VectorXf& p1
)
{
    assert(
        p0.cols() == p1.cols() &&
        p0.rows() == p1.rows() &&
        "The size of two points must be same."
    );
    return (p0-p1).norm();
}

template <typename T>
T angle(
    const cv::Point_<T>& p0,
    const cv::Point_<T>& p1
)
{
    double val = p0.dot(p1) / (cv::norm(p0)*cv::norm(p1));
    if(val<0.0)
    {
        val = 0.0;
    }
    else if(val>1.0)
    {
        val = 1.0;
    }
    return static_cast<T>(val);
}

float angle(
    const Eigen::VectorXf& p0,
    const Eigen::VectorXf& p1
)
{
    return (p0.dot(p1)) / (p0.norm()*p1.norm());
}

///////////////////////////////////////////
/// Point Set using Eigen
PointSet::PointSet() :
    size(0),
    sizePair(0)
{
}

PointSet::PointSet(
    const std::vector<cv::Point2f>& _points
)
{
    Points(_points);
}

PointSet::PointSet(
    const std::vector<cv::KeyPoint>& _points
)
{
    Points(_points);
}

PointSet::~PointSet()
{
}

void PointSet::Points(
    const std::vector<cv::Point2f>& _points
)
{
    size = int(_points.size());
    points = Eigen::MatrixXf::Zero(2, size);
    for(int n = 0; n < size; ++n)
    {
        points(0,n) = _points[n].x;
        points(1,n) = _points[n].y;
    }
    sizePair = (size*(size-1))/2;
    computeDistance();
}

void PointSet::Points(
    const std::vector<cv::KeyPoint>& _points
)
{
    int _size = int(_points.size());
    std::vector<cv::Point2f> _tmp(_size);
    for(int n = 0; n < _size; ++n)
    {
        _tmp[n] = _points[n].pt;
    }
    Points(_tmp);
}

Eigen::MatrixXf PointSet::Points(void) const
{
    return points;
}

Eigen::VectorXf PointSet::Points(
    const int id
) const
{
    assert(
        id >= 0 &&
        id < size &&
        "the index of the point must be in range."
    );
    return points.col(id);
}

Eigen::MatrixXf PointSet::Points(
    const std::vector<int> id
) const
{
    int _size = int(id.size());
    Eigen::MatrixXf _points(2, _size);
    for(int n = 0; n < _size; ++n)
    {
        _points.col(n) = Points(id[n]);
    }
    return _points;
}

void PointSet::Points(
    Eigen::MatrixXf & _points) const
{
    _points = points;
}

void PointSet::computeDistance(void)
{
    assert(
        size > 1 &&
        "The point set must have 2 points."
    );

    int size2 = square(size);
    distances = std::vector<float>(size2);
    int i,j;
    for(int n = 0; n < size2; ++n)
    {
        i = n/size;
        j = n%size;
        distances[n] = distance(points.col(i), points.col(j));
    }
}

float PointSet::Distances(
    const int i,
    const int j
) const
{
    assert(
        i >= 0 &&
        i < size &&
        j >= 0 &&
        j < size &&
        "the index of the points must be in range."
    );
    if(i<=j) return distances[i*size+j];
    else    return distances[j*size+i];
}

float PointSet::Distances(
    const int id
) const
{
    assert(
        id >= 0 &&
        id < sizePair &&
        "the index of the points must be in range."
    );
    return distances[id];
}

///////////////////////////////////////////
/// Correspondence.
//! Default constructor with reference point set size.
Correspondence::Correspondence():
    idReference(-1),
    idQuery(-1),
    affinity(0.0f)
{
}

Correspondence::Correspondence(
    const int i,
    const int j,
    const float a
):
    idReference(i),
    idQuery(j),
    affinity(a)
{
}

Correspondence::~Correspondence()
{
}

void Correspondence::set(
    const int i,
    const int j,
    const float a
)
{
    idReference = i;
    idQuery = j;
    affinity = a;
}

///////////////////////////////////////////
/// some utility functions

template <typename TypeMatrix>
void _PowerIteration(
    const TypeMatrix& matAffinity,
    const int& numIterationMaximum,
    const bool flagDebug,
    int& convergenceCondition,
    int& numIteration,
    Eigen::VectorXf& v_k
)
{
    assert(
        matAffinity.rows() == matAffinity.cols() &&
        "The affinity matrix must be square matrix and the number of rows/columns must be equal to the number of all potential correspondences."
    );
    if(flagDebug)
    {
        std::cout << "Power Iteration:" << std::endl;
    }
    int numCorrespondences = matAffinity.rows();

    // initialize vectors
    v_k = Eigen::VectorXf::Random(numCorrespondences).cwiseAbs();
    Eigen::VectorXf v_k_1 = Eigen::VectorXf::Random(numCorrespondences).cwiseAbs();
    Eigen::VectorXf v_k_2 = Eigen::VectorXf::Random(numCorrespondences).cwiseAbs();

    float eps = std::numeric_limits<float>::epsilon();
    numIteration = 0;

    if(flagDebug)
    {
        std::cout << "The input affinity matrix:" << std::endl;
        std::cout << matAffinity << std::endl << std::endl;
        std::cout << "  v_0:   " << v_k.transpose() << std::endl;
    }
    while(1)
    {
        if(++numIteration > numIterationMaximum)
        {
            convergenceCondition = 0;
            break;
        }
        else if( ((v_k-v_k_1).norm() < eps) && ((v_k-v_k_2).norm() < eps) )
        {
            convergenceCondition = 1;
            break;
        }
        // copy the previous solutions
        v_k_2 = v_k_1;
        v_k_1 = v_k;
        // v_k = M*v_k
        v_k = matAffinity*v_k;
        // v_k = v_k / ||v_k||
        v_k = v_k.normalized();
        if(flagDebug)
        {
            std::cout << "  v_" << numIteration << "  :   " << v_k.transpose() << std::endl;
            std::cout << "  norm(v_k-v_k_1): " << (v_k-v_k_1).norm() << std::endl;
            std::cout << "  norm(v_k-v_k_2): " << (v_k-v_k_2).norm() << std::endl;
        }
    }

	if (flagDebug)
	{
		std::cout << "  v_end : " << v_k.transpose() << std::endl;
	}
}

void _HungarianAlgorithm(
    const Eigen::MatrixXf& matHungarian,
    const bool flagDebug,
    std::vector<int>& index0,
    std::vector<int>& index1,
    std::vector<float>& similarity
)
{
    if(flagDebug)
    {
        std::cout << "Hungarian algorithm:" << std::endl;
    }

    // apply Hungarian method
    Eigen::MatrixXf M = matHungarian;
    int maxRow, maxCol;
    float valMax;

    index0.clear();
    index1.clear();
    similarity.clear();

	if (flagDebug)
	{
		std::cout << "  Eigenvector matrix:" << std::endl << M << std::endl;
	}

    while(1)
    {
        valMax = M.maxCoeff(&maxRow, &maxCol);
        if(valMax < 0.0)
        {
            break;
        }
        index0.push_back( maxRow );
        index1.push_back( maxCol );
        similarity.push_back( valMax );
        M.row(maxRow).setConstant(-1.0f);
        M.col(maxCol).setConstant(-1.0f);
        if(flagDebug)
        {
            std::cout << "  match[" << similarity.size() << "] = (" << maxRow << "," << maxCol << ")" << std::endl;
            std::cout << "  mat:" << std::endl << M << std::endl;
        }
    }

	if (flagDebug)
	{
		std::cout << std::endl << "  index0: ";
		for (int n = 0; n < index0.size(); ++n)  std::cout << index0[n] << ",";
		std::cout << std::endl << "  index1: ";
		for (int n = 0; n < index1.size(); ++n)  std::cout << index1[n] << ",";
		std::cout << std::endl;
	}
}

void _decodeAssignmentIndicator(
    const std::vector<int>& index0,
    const std::vector<int>& index1,
    const std::vector<float>& similarity,
    Correspondences& _matchings
)
{
    assert(
        index0.size() == index1.size() &&
        index0.size() == similarity.size() &&
        "The size of indices and similarities must be same."
    );
    int size = int(index0.size());
    _matchings = Correspondences(size);
    for(int n = 0; n < size; ++n)
    {
        _matchings[n] = Correspondence(index0[n], index1[n], similarity[n]);
    }
}

void _decodeAssignmentIndicator(
    const std::vector<int>& index0,
    const std::vector<int>& index1,
    const std::vector<float>& similarity,
    const std::vector<int>& numPoints0,
    std::vector< Correspondences >& _matchings
)
{
    assert(
        index0.size() == index1.size() &&
        index0.size() == similarity.size() &&
        "The size of indices and similarities must be same."
    );

    int sizeSets = int(numPoints0.size());
    _matchings = std::vector<Correspondences>(sizeSets);
    std::vector<int> index = numPoints0;
    for(int n = 1; n < sizeSets; ++n)
    {
        index[n] += index[n-1];
    }
    int size = int(index0.size());
    int indexPointset = 0;
    for(int n = 0; n < size; ++n)
    {
        for(int m = 0; m < sizeSets; ++m)
        {
            if(index0[n]/index[m] == 0)
            {
                indexPointset = m;
                if(indexPointset == 0)
                {
                    _matchings[indexPointset].push_back(Correspondence(index0[n], index1[n], similarity[n]));
                }
                else
                {
                    _matchings[indexPointset].push_back(Correspondence(index0[n]%index[indexPointset-1], index1[n], similarity[n]));
                }
                break;
            }
        }
    }
}

void _ConvergenceCondition(
    const int convergenceCondition,
    const int numIteration
)
{
    std::cout << "  power method converged: ";
    switch(convergenceCondition)
    {
    case 0:
        std::cout << "reached maximum number of iterations";
        break;
    case 1:
        std::cout << "eigenvector found with " << numIteration << " iterations";
        break;
    default: // -1, not run the power method
        std::cout << "not converged";
    }
    std::cout << std::endl;
}

void _matrixizeVector(
    const Eigen::VectorXf& v,
    const int& cols,
    Eigen::MatrixXf& m
)
{
    int rows = int(v.rows())/cols;
    assert(
        rows*cols == int(v.rows()) &&
        "The size of the vector must be valid."
    );
    m = Eigen::MatrixXf::Zero(rows, cols);

    for(int i = 0; i < rows; ++i)
    {
        for(int j = 0; j < cols; ++j)
        {
            m(i, j) = v[i*cols+j];
        }
    }
}

void _concatenateMatrices(
    std::vector< Eigen::VectorXf >& vecEigen,
    const std::vector<int>& _numPointsReference,
    const int& _numPointsQuery,
    Eigen::MatrixXf& matHungarian
)
{
    int rows = std::accumulate(_numPointsReference.begin(), _numPointsReference.end(), 0);
    int cols = _numPointsQuery;
    matHungarian = Eigen::MatrixXf::Zero(rows, cols);

    int numPointsets = int(_numPointsReference.size());
    int row0 = 0;
    for(int n = 0; n < numPointsets; row0+=_numPointsReference[n], ++n)
    {
        for(int i = 0; i < _numPointsReference[n]; ++i)
        {
            for(int j = 0; j < _numPointsQuery; ++j)
            {
                matHungarian(i+row0, j) = vecEigen[n][i*_numPointsQuery+j];
            }
        }
    }
}

///////////////////////////////////////////
/// SpectralMethod
SpectralMethod::SpectralMethod():
    numPointsReference(std::vector<int>(0)),
    numPointsQuery(0),
    numCorrespondences(std::vector<int>(0)),
    numIterationMaximum(100),
    convergenceCondition(0),
    flagDebug(false)
{
}

SpectralMethod::~SpectralMethod()
{
}

void SpectralMethod::NumPointsReference(
    const std::vector<int>& n
)
{
    assert(
        n.size() > 0 &&
        "The number of reference point sets must be natural number."
    );
    numPointsReference = n;
}

void SpectralMethod::NumPointsReference(
    const int n,
    const int m
)
{
    assert(
        numPointsReference.size() > n &&
        "The number of reference point sets must be natural number."
    );
    numPointsReference[n] = m;
}

void SpectralMethod::NumPointsQuery(
    const int n
)
{
    assert(
        n>0 &&
        "The number of query points must be natural number."
    );
    numPointsQuery = n;
}

void SpectralMethod::NumIterationMaximum(
    const int n
)
{
    assert(
        n>0 &&
        "The number of maximum iteration must be natural number."
    );
    numIterationMaximum = n;
}

std::vector<Correspondences> SpectralMethod::Matchings(void) const
{
    return matchings;
}

Correspondences SpectralMethod::Matchings(
    const int n
) const
{
    assert(
        n >= 0 &&
        n < matchings.size() &&
        "The specified index of the matchings must be natural number and less than the number of all the matchings"
    );
    return matchings[n];
}

Correspondence SpectralMethod::Matchings(
    const int n,
    const int m
) const
{
    assert(
        n >= 0 &&
        n < matchings.size() &&
        "The specified index of the matchings must be natural number and less than the number of all the matchings"
    );
    assert(
        m >= 0 &&
        m < matchings[n].size() &&
        "The specified index of the matchings must be natural number and less than the number of all the matchings"
    );
    return matchings[n][m];
}

void SpectralMethod::Matching(
    const std::vector< std::vector<cv::Point2f> >& pointsReference,
    const std::vector<cv::Point2f>& pointsQuery,
    const int methodAffinity,
    std::vector<Correspondences>& _matchings
)
{
    assert(
        pointsReference.size() > 0 &&
        pointsQuery.size() > 0 &&
        "The size of the pointsets must be greather than 0."
    );
    numPointsQuery = int(pointsQuery.size());
    numPointsetsReference = int(pointsReference.size());
    numPointsReference = std::vector<int>(numPointsetsReference, 0);
    numCorrespondences = std::vector<int>(numPointsetsReference, 0);
    std::vector<int> startIndex(numPointsetsReference, 0);
    for(int n = 0; n < numPointsetsReference; ++n)
    {
        numPointsReference[n] = int(pointsReference[n].size());
        numCorrespondences[n] = numPointsReference[n]*numPointsQuery;
        if(n != 0)
        {
            startIndex[n] = startIndex[n-1] + numPointsReference[n-1];
        }
    }

    // construct affinity matrix
    int size = (startIndex[numPointsetsReference-1]+numPointsReference[numPointsetsReference-1]) * numPointsQuery;
    std::cout << "matrix size = " << "(" << startIndex[numPointsetsReference-1] << "+" << numPointsReference[numPointsetsReference-1] << ")*" << numPointsQuery << std::endl;
    Eigen::SparseMatrix<float> matAffinity(size, size);
    Affinity a;
    a.MethodAffinity(methodAffinity);
    a.computeAffinity(
        pointsReference,
        pointsQuery,
        matAffinity
    );

    // run power iteration
    Eigen::VectorXf vecEigen;
    _PowerIteration(
        matAffinity,
        numIterationMaximum,
        flagDebug,
        convergenceCondition,
        numIteration,
        vecEigen
    );
    _ConvergenceCondition(
        convergenceCondition,
        numIteration
    );

    // run hungarian algorithm
    Eigen::MatrixXf matHungarian;
    std::vector<int> indexReference, indexQuery;
    std::vector<float> similarity;
    _matrixizeVector(
        vecEigen,
        numPointsQuery,
        matHungarian
    );
    _HungarianAlgorithm(
        matHungarian,
        flagDebug,
        indexReference,
        indexQuery,
        similarity
    );
    _decodeAssignmentIndicator(
        indexReference,
        indexQuery,
        similarity,
        numPointsReference,
        _matchings
    );
}

void SpectralMethod::MatchingIndependent(
    const std::vector< std::vector<cv::Point2f> >& pointsReference,
    const std::vector<cv::Point2f>& pointsQuery,
    const int methodAffinity,
    std::vector<Correspondences>& _matchings
)
{
    assert(
        pointsReference.size() > 0 &&
        pointsQuery.size() > 0 &&
        "The size of the pointsets must be greather than 0."
    );
    numPointsQuery = int(pointsQuery.size());
    numPointsetsReference = int(pointsReference.size());
    numPointsReference = std::vector<int>(numPointsetsReference, 0);
    numCorrespondences = std::vector<int>(numPointsetsReference, 0);
    _matchings = std::vector<Correspondences>(numPointsetsReference);
    for(int n = 0; n < numPointsetsReference; ++n)
    {
        numPointsReference[n] = int(pointsReference[n].size());
        numCorrespondences[n] = numPointsReference[n]*numPointsQuery;
    }

    int numThreads = 1;
    int tid = 0;
#ifdef _OPENMP
    numThreads = omp_get_max_threads();
#endif
#ifdef _OPENMP
#pragma omp parallel
    {
        tid = omp_get_thread_num();
#endif
        for(int n = tid; n < numPointsetsReference; n+=numThreads)
        {
            int size = numPointsReference[n] * numPointsQuery;
            std::cout << "matrix[" << n << "] size = " << size << "*" << numPointsQuery << std::endl;
            Eigen::SparseMatrix<float> matAffinity(size, size);
            // construct affinity matrix
            Affinity a;
            a.MethodAffinity(methodAffinity);
            a.computeAffinity(
                pointsReference[n],
                pointsQuery,
                matAffinity
            );

            // run power iteration
            Eigen::VectorXf vecEigen;
            _PowerIteration(
                matAffinity,
                numIterationMaximum,
                flagDebug,
                convergenceCondition,
                numIteration,
                vecEigen
            );
            _ConvergenceCondition(
                convergenceCondition,
                numIteration
            );

            // run hungarian algorithm
            Eigen::MatrixXf matHungarian;
            std::vector<int> indexReference, indexQuery;
            std::vector<float> similarity;
            _matrixizeVector(
                vecEigen,
                numPointsQuery,
                matHungarian
            );
            _HungarianAlgorithm(
                matHungarian,
                flagDebug,
                indexReference,
                indexQuery,
                similarity
            );
            _decodeAssignmentIndicator(
                indexReference,
                indexQuery,
                similarity,
                _matchings[n]
            );
        }
#ifdef _OPENMP
    }
#endif
}
