/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada and Hideaki Uchiyama.

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   MarkerGeneration.h
@brief  core to generate, save, and load random dot markers.
@author Yuji Oyamada
*/

#ifndef MARKERGENERATOR_H
#define MARKERGENERATOR_H

#include <cmath>
#include <vector>
#include <iostream>
#include <sstream>
#include <list>
#include <random>
// for ImageMagick API
#include <Magick++.h>
// for Eigen
#include <Eigen/Core>
#include <Eigen/Geometry>
// for data output
#include <fstream>

namespace Scale{
double mm2inch(const double mm);
double inch2mm(const double inch);
double inch2pixel(const double inch, const double dpi=72.0);
double mm2pixel(const double mm, const double dpi=72.0);
double pixel2inch(const double pixel, const double dpi=72.0);
double pixel2mm(const double pixel, const double dpi=72.0);
} // end of namespace Scale

class RandomDotsMarkerGenerator
{
public:
/// @brief A constructor
	RandomDotsMarkerGenerator(void);
/// @brief A destructor
	~RandomDotsMarkerGenerator(void);

/// @brief sets all variables
    void SetAllVariables(void);

    /// @brief shows current variables
    void ShowAllVariables(void);

    /// @brier makes a set of markers with current settings
    void MakeMarkerSets(
        const std::string basename,
        const int numMarker = 5
    );
/// @brief Set marker width
// @paramin ImageWidth
// @par Modify m_imageWidth
    void SetWidth(const int ImageWidth){
        m_imageWidth = ImageWidth;
        distX = std::uniform_real_distribution<>(m_dotMargin, m_imageWidth-m_dotMargin);
    }

/// @brief Set marker height
// @paramin ImageHeight
// @par Modify m_imageHeight
    void SetHeight(const int ImageHeight){
        m_imageHeight = ImageHeight;
        distY = std::uniform_real_distribution<>(m_dotMargin, m_imageHeight-m_dotMargin);
    }

/// @brief Set number of dots
// @paramin NumOfDots
// @par Modify m_numOfDots
    void SetNumOfDots(const int NumOfDots){
        m_numOfDots = NumOfDots;
        m_points = Eigen::MatrixXd(2, m_numOfDots);
        m_indices.clear();
    }

/// @brief Set distance between dots
// @paramin DotsMargin
// @par Modify m_dotMargin
    void SetDotsMargin(const int DotsMargin){ m_dotMargin = DotsMargin; }

/// @brief Set dot radius
// @paramin DotRadius
// @par Modify m_dotRadius
    void SetDotRadius(const int DotRadius){ m_dotRadius = DotRadius; }

/// @brief Set dot color
// @paramin DotsColor
// @par Modify m_dotColor
    void SetDotsColor(const std::string DotsColor){ m_dotColor = DotsColor; }

/// @brief Set background color
// @paramin bgColor
// @par Modify m_bgColor
    void SetBgColor(const std::string bgColor){ m_bgColor = bgColor; }

/// @brief Batch marker generation steps
    void MakeMarker(const std::string filename);

/// @brief Save marker
// @paramin name
    void SaveMarkerAsImage(const std::string name);

/// @brief  Save a set of dots position as .points file
// @paramin name
    void SaveText(const std::string name);

/// @brief  Save a set of dots position as .xml file
    // @paramin name
        void SaveMarkerAsXml(const std::string fileName);

    // @paramin name
        void SaveMarkersAsXml(const std::string fileName,
                              const std::vector<Eigen::MatrixXd>& markers);
	
/// @brief  Load a set of dots position from .xml file
// @paramin name
    bool LoadMarkerFromXml(const std::string fileName,
                           int& width,
                           int& height,
                           Eigen::MatrixXd& points);

/// @brief Get marker width
// @paramout m_imageWidth
    int GetWidth( ) const { return m_imageWidth; }

/// @brief Get marker height
// @paramout m_imageHeight
    int GetHeight( ) const { return m_imageHeight; }

/// @brief Get number of dots
// @paramout m_numOfDots
    int GetNumOfDots( ) const { return m_numOfDots; }

/// @brief Get DPI
// @paramout m_dpi
    int GetDpi( ) const { return m_dpi; }

/// @brief Get distance between dots
// @paramout m_dotMargin
    int GetDotMargin( ) const { return m_dotMargin; }

/// @brief Get dot's radius
// @paramout m_dotRadius
    int GetDotRadius( ) const { return m_dotRadius; }

/// @brief Get dot's color
// @paramout m_dotColor
    std::string GetDotColor( ) const { return m_dotColor; }

/// @brief Get background color
// @paramout m_bgColor
    std::string GetBgColor( ) const { return m_bgColor; }

    Eigen::MatrixXd GetPoints() const {return m_points; }
private:
    enum eMarkerProperty{
        MARKER_WIDTH,
        MARKER_HEIGHT,
        MARKER_NUMOFDOTS,
        MARKER_DOTSMARGIN,
        MARKER_DOTRADIUS,
        MARKER_DOTSCOLOR,
        MARKER_BGCOLOR,
        MARKER_NUMPROPERTY
    };
	Magick::Image m_img;					//!< @brief 
	int m_dpi;          					//!< @brief DPI(Dots Per Inch) of marker
    size_t m_imageWidth;                	//!< @brief marker width
    size_t m_imageHeight;                   //!< @brief marker height
	size_t m_numOfDots;						//!< @brief number of dots
	int m_dotMargin;						//!< @brief minimum margin between dot centers
	int m_dotRadius;						//!< @brief dot size
	std::string m_dotColor;					//!< @brief dot color
	std::string m_bgColor;					//!< @brief background color
	std::list<Magick::Drawable> m_drawList;	//!< @brief
    Eigen::MatrixXd m_points;         	//!< @brief a set of 3d positions of control points
    std::vector<size_t> m_indices;		//!< @brief a set of indices of control points

    std::random_device rd;
    std::default_random_engine engine;
    std::uniform_real_distribution<> distX;
    std::uniform_real_distribution<> distY;

	void Init();

/// @brief Check marker size
	void CheckImageSize();

/// @brief Check how many dots are generated
	void CheckNumOfDots();

/// @brief Compute a set of dot centers
// Due to poor implementation, this function will never converge in worst case.<br>
// In such case, please
// - reduce m_numOfDots, m_dotMargin
// - increase m_imageWidth, m_imageHeight
    void GenerateDots(Eigen::MatrixXd& points);

/// @brief Draw marker origin
	void DrawOrigin();

    void DrawScale();

    void SetCurrentValue(
        const int caseVariable,
        const std::string varStr
    );

    void ShowCurrentValue(
        const int caseVariable
    );

};

#endif
