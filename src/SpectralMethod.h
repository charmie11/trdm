/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   SpectralMethod.h
@brief  the core implementation of spectral method.
@author Yuji Oyamada
*/

#pragma once

#include "AffinityMatrix.h"
#include <numeric>

//! computes the square of \c x.
template <typename T>
T square(const T x);

//! computes the distance between two points \c p0 and \c p1.
float distance(
    const Eigen::VectorXf& p0,
    const Eigen::VectorXf& p1
);

//! computes the angle between two points \c p0 and \c p1.
template <typename T>
T angle(
    const cv::Point_<T>& p0,
    const cv::Point_<T>& p1
);

//! computes the angle between two points \c p0 and \c p1.
float angle(
    const Eigen::VectorXf& p0,
    const Eigen::VectorXf& p1
);

///
/// \brief The PointSet struct
///
struct PointSet{
    //! The default constructor.
    PointSet();
    //! Constructor with a set of cv::Point2f.
    PointSet(const std::vector<cv::Point2f>& _points);
    //! Constructor with a set of cv::KeyPoint.
    PointSet(const std::vector<cv::KeyPoint>& _points);
    //! Destructor.
    ~PointSet();

    ///
    /// \brief Points sets a new point set.
    /// \param _points  the set of points of type \c cv::Point2f to be set.
    ///
    void Points(const std::vector<cv::Point2f>& _points);
    ///
    /// \brief Points sets a new point set.
    /// \param _points  the set of points of type \c cv::KeyPoint to be set.
    ///
    void Points(const std::vector<cv::KeyPoint>& _points);

    ///
    /// \brief Points returns the set of all points.
    /// \return     \c points
    ///
    Eigen::MatrixXf Points() const;

    ///
    /// \brief Points returns \c id-th point.
    /// \param id the index of the point of interest.
    /// \return     \c points[id]
    ///
    Eigen::VectorXf Points(const int id) const;

    ///
    /// \brief Points returns a subset of points of interest.
    /// \param id the indices of the points of interest.
    /// \return     subset of \c points
    ///
    Eigen::MatrixXf Points(const std::vector<int> id) const;

    ///
    /// \brief Points returns \c id-th point.
    /// \param id the index of the point of interest.
    /// \return     \c points[id]
    ///
    void Points(Eigen::MatrixXf& _points) const;

    ///
    /// \brief Distances returns a set of distances.
    /// \return the set of distances.
    ///
    std::vector<float> Distances(void) const {return distances;}

    ///
    /// \brief Distances returns a set of distances.
    /// \return the set of distances.
    ///
    std::vector<float>& Distances(void){return distances;}

    ///
    /// \brief Distances sets a set of distances \c distances as \c _distances.
    /// \return the set of distances.
    ///
    void Distances(std::vector<float>& _distances) const {_distances = distances;}

    //! returns the distance between \c points[i] and \c points[j].
    float Distances(const int i, const int j) const ;
    //! returns the distance between \c points[id/size] and \c points[id%size].
    float Distances(const int id) const ;

    //! computes the distance between 2 points.
    void computeDistance(void);

    int size;                           //!< The number of points.
    int sizePair;                       //!< The number of point pairs.
    Eigen::MatrixXf points;
    std::vector<float> distances;       //!< A set of distances between two points
};

///
/// \brief The Correspondence struct
///
struct Correspondence
{
    //! Default constructor.
    Correspondence();
    //! Constructor with a triplet (i,j,a).
    Correspondence(const int i, const int j, const float a = 0.0f);
    //! Destructor.
    ~Correspondence();

    //! compares this Correspondence and the given Correspondence \c c based on their indices of reference \c idReference.
    bool operator<(const Correspondence&c) const
    {
        return this->idReference < c.idReference;
    }

    //! sets a correspondence \c (i,j) and set its affinity as \c a.
    void set(const int i, const int j, const float a);

    //! returns the reference index.
    int IdReference(void) const {return idReference;}
    //! returns the query index.
    int IdQuery(void) const {return idQuery;}
    //! returns the affinity.
    float Affinity(void) const {return affinity;}

    int idReference;    //!< The index of reference points.
    int idQuery;        //!< The index of query points.
    float affinity;     //!< The affinity between the reference point and the query point
};
///
/// \brief The Correspondences struct
///
using Correspondences = std::vector<Correspondence>;

///
/// \brief PowerIteration computes the eigenvector, corresponding to the maximum eigenvalue in absolute value, of the input affinity matrix \c matAffinity with the power iteration method.
/// \param matAffinity          The input affinity matrix.
/// \param numIterationMaximum  The maximum number of iterations.
/// \param flagDebug            The flag turning on/off debugging message.
/// \param convergenceCondition The type of convergence.
/// \param numIteration         The number of iterations.
/// \param v_k                  The eigenvector corresponding to the maximum eigenvalue in absolute value.
///
template <typename TypeMatrix>
void _PowerIteration(
    const TypeMatrix& matAffinity,
    const int& numIterationMaximum,
    const bool flagDebug,
    int& convergenceCondition,
    int& numIteration,
    Eigen::VectorXf& v_k
);

///
/// \brief _HungarianAlgorithm finds a set of point-to-point correspondences giving a matrixized eigenvector with the Hungarian algorithm.
/// \param matHungarian The eigenvector as an matrix.
/// \param flagDebug    The flag turning on/off debugging message.
/// \param index0       THe indices of the reference points.
/// \param index1       THe indices of the query points.
/// \param similarity   The similarity measure.
///
void _HungarianAlgorithm(
    const Eigen::MatrixXf& matHungarian,
    const bool flagDebug,
    std::vector<int>& index0,
    std::vector<int>& index1,
    std::vector<float>& similarity
);

///
/// \brief _decodeAssignmentIndicator converts the indices of reference points and query points to \c Correspondences objects.
/// \param index0       THe indices of the reference points.
/// \param index1       THe indices of the query points.
/// \param similarity   The similarity measure.
/// \param matchings    The set of point-to-point correspondences.
///
void _decodeAssignmentIndicator(
    const std::vector<int>& index0,
    const std::vector<int>& index1,
    const std::vector<float>& similarity,
    Correspondences& _matchings
);

///
/// \brief _decodeAssignmentIndicator converts the indices of reference points and query points to \c cv::DMatch objects.
/// \param index0       THe indices of the reference points.
/// \param index1       THe indices of the query points.
/// \param similarity   The similarity measure.
/// \param numPoints0   The numbers of reference points.
/// \return             The set of point-to-point correspondences.
///
void _decodeAssignmentIndicator(
    const std::vector<int>& index0,
    const std::vector<int>& index1,
    const std::vector<float>& similarity,
    const std::vector<int>& numPoints0,
    std::vector< Correspondences >& _matchings
);

///
/// \brief _ConvergenceCondition shows the satisfied convergence condition of the power method.
/// \param convergenceCondition The indicator of the convergence type.
/// \param numIteration         The number of iterations.
///
void _ConvergenceCondition(
    const int convergenceCondition,
    const int numIteration = 0
);

///
/// \brief _concatenateMatrices concatenates a set of eigenvectors and converts the concatenated vector to a matrix.
/// \param v    The eigenvector.
/// \param cols The size of query point set.
/// \param m    The obtained matrix.
///
void _matrixizeVector(
    const Eigen::VectorXf& v,
    const int& cols,
    Eigen::MatrixXf& m
);

///
/// \brief _concatenateMatrices concatenates a set of eigenvectors and converts the concatenated vector to a matrix.
/// \param vecEigen             The set of eigenvectors.
/// \param _numPointsReference  The size of reference poitn sets.
/// \param _numPointsQuery      The size of query point set.
/// \param matHungarian         The obtained matrix.
///
void _concatenateMatrices(
    std::vector< Eigen::VectorXf >& vecEigen,
    const std::vector<int>& _numPointsReference,
    const int& _numPointsQuery,
    Eigen::MatrixXf& matHungarian
);

///
/// @class  SpectralMethod
/// @brief The SpectralMethod class finds a set of point-to-point correspondences given an affinity matrix.
/// The Spectral Method (SM) was proposed by Leodeanu and Hebert at ICCV 2005.
///
/// Suppose there are two point sets, reference and query,
/// each of which contains \c numPointsReference and \c numPointsQuery points respectively.
/// The number of all potential point-to-point correspondences is denoted by \c numCorrespondences.
///
/// SM takes 2 steps to find a set of matchings \c matchings given an affinity matrix.
/// The affinity matrix must be a square matrix and the number of rows/columns must be equal to \c numCorrespondences = \c numPointsReference * \c numPointsQuery.
///
/// The first step is executed by the function \c PowerIteration that finds the eigenvector of the affinity matrix that corresponds to the eivenvalue of the maximum absolute value.
/// As the function name tells, the function applies the Power Iteration method on the affinity matrix and returns the eigenvector.
///
/// The second step is executed by the function \c HungarianAlgorithm that solves the matcning problem as an assignment problem.
/// Giving the eivenvector obtained by the Power Iteration, Hungarian algorithm finds an appropriate set of point-to-point correspondences \c matchings.
///
/// The matching result is stored as \c Correspondences object \c matchings.
/// The n-th point-to-point correspondence is denoted by \c matchings[n] and
/// the correspondence represents the matching between \c matchings[n].trainIdx-th reference point and \c matchings[n].queryIdx-th query point.
/// The set of indices are also stored independently as \c std::vector<int> objects, \c indexReference and \c indexQuery.
/// \c matchings[n].trainIdx = \c indexReference[n] and \c matchings[n].queryIdx = \c indexQuery[n].
///
class SpectralMethod{
public:
    //! Default constructor
    SpectralMethod();
    //! Destructor
    ~SpectralMethod();

    ///
    /// \brief Matching runs the spectral method given the reference pointsets \c pointsReference and the query pointset \c pointsQuery
    /// \param pointsReference  The reference pointset.
    /// \param pointsQuery      The query pointset.
    /// \param methodAffinity   The indicator how affinity is computed.
    /// \param _matchings       The matching result as \c std::vector<Correspondences>.
    ///
    void Matching(
        const std::vector< std::vector<cv::Point2f> >& pointsReference,
        const std::vector<cv::Point2f>& pointsQuery,
        const int methodAffinity,
        std::vector<Correspondences>& _matchings
    );

    ///
    /// \brief MatchingIndependent runs the spectral method given the reference pointsets \c pointsReference and the query pointset \c pointsQuery independently.
    /// \param pointsReference  The reference pointset.
    /// \param pointsQuery      The query pointset.
    /// \param methodAffinity   The indicator how affinity is computed.
    /// \param _matchings       The matching result as \c std::vector<Correspondences>.
    ///
    void MatchingIndependent(
        const std::vector< std::vector<cv::Point2f> >& pointsReference,
        const std::vector<cv::Point2f>& pointsQuery,
        const int methodAffinity,
        std::vector<Correspondences>& _matchings
    );

    //! sets the number of reference points \c numPointsReference to \c n.
    void NumPointsReference(const std::vector<int>& n);
    //! sets the number of reference points \c numPointsReference to \c n.
    void NumPointsReference(const int n, const int m);
    //! sets the number of query points \c numPointsQuery to \c n.
    void NumPointsQuery(const int n);
    //! sets the number of maximum iterations for the power method \c numIterationMaximum to \c n.
    void NumIterationMaximum(const int n);
    //! sets the debug flag \c flagDebug to \c f.
    void FlagDebug(const bool f){flagDebug = f;}

    //! returns all point-to-point correspondences \c matchings between the reference point sets and the query point set.
    std::vector< Correspondences > Matchings(void) const;
    //! returns all point-to-point correspondences \c matchings between n-th reference point set and the query point set.
    Correspondences Matchings(const int n) const;
    //! returns \c m-th point-to-point correspondence \c matchings between n-th reference point set and the query point set.
    Correspondence Matchings(const int n, const int m) const;
private:
    int numPointsetsReference;              //!< The number of reference point sets.
    std::vector<int> numPointsReference;    //!< The number of points in the reference point sets.
    int numPointsQuery;                     //!< The number of query points.
    std::vector<int> numCorrespondences;    //!< The number of all potential correspondences.
    int numIterationMaximum;                //!< The number of maximum iteration for the power method.
    int numIteration;          //!< The number of maximum iteration for the power method.
    int convergenceCondition;  //!< The convergence condition of the power method.
    std::vector<Correspondences> matchings;      //!< A set of point-to-point correspondences.
    bool flagDebug;                         //!< A flag indicating debug/release mode.
};
