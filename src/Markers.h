/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   Markers.h
@brief  to generate TRDM markers.
@author Yuji Oyamada
*/

#pragma once

#include <string>
#include <vector>

#include <opencv2/opencv.hpp>

struct paramMarkers{
    // parameter for all markers
    int num_markers;
    // parameter for a marker
    int image_width;
    int image_height;
    int num_points;
};

bool load_config_genmarker(
    const std::string& filename,
    paramMarkers& param
);

void print_markers(const std::vector < std::vector< cv::Point2f > >& markers);

std::vector< std::vector< cv::Point2f > > generate_markers(const paramMarkers param);

bool save_markers_as_xml(
    const std::vector< std::vector< cv::Point2f > >& markers,
    const paramMarkers param,
    const std::string filename
);

bool load_markers_from_xml(
    const std::string filename,
    std::vector< std::vector< cv::Point2f > >& markers,
    paramMarkers& param
);

bool test_save_load(
    const std::vector< std::vector< cv::Point2f > >& markers,
    const paramMarkers param
);

void save_markers_as_image(
    const std::vector< std::vector< cv::Point2f > >& markers,
    const paramMarkers param,
    const std::string dirname,
    const std::string prefix
);
