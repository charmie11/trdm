/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Hideaki Uchiyama.

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   Combination.h
@brief  to compute combinations.
@author Hideaki Uchiyama
*/

#pragma once

#include <vector>

/*!
@class		compute combinations
*/
class CCombination
{
public:
	static std::vector< std::vector<int> > GetCombination(const int n, const int m);
	static std::vector< std::vector<int> > GetAllRotatedCombination(const int n, const int m);

private:
	static void compute(int nest, int column, int n1, int n2, int k[], std::vector< std::vector<int> > &result);
};

