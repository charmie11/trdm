/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada and Hideaki Uchiyama.

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   demoMarkerGeneration.cpp
@brief  sample c++ program to generate TRDM markers.
@author Yuji Oyamada
*/
#include "Markers.h"

int main(int argc, char **argv)
{
    paramMarkers param;
    std::string filename_config = "../data/gen_markers.xml";
    if(!load_config_genmarker(filename_config, param))
    {
        std::cerr << "load_config() failed!" << std::endl;
        return -1;
    }
    std::cout << "load_config() passed!" << std::endl;
    std::cout << "    #markers: " << param.num_markers << std::endl;
    std::cout << "    image_width: " << param.image_width << std::endl;
    std::cout << "    image_height: " << param.image_height << std::endl;
    std::cout << "    #points: " << param.num_points << std::endl;

    // generate markers
    auto markers = generate_markers(param);
    print_markers(markers);

    // save the generated markers as an xml file
    std::string filename_xml = "../data/markers.xml";
    std::cout << "saving the markers..." << std::endl;
    save_markers_as_xml(markers, param, filename_xml);
    if(!test_save_load(markers, param))
    {
        std::cerr << "either or both of save and load functions failed" << std::endl;
    }

    // save the generated markers as png files
    save_markers_as_image(markers, param, "../data/", "markers");

    return 0;
}
