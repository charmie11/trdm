/*!
    This file is part of TRDM (https://gitlab.com/charmie11/trdm/).
    Copyright (c) 2019 Yuji Oyamada and Hideaki Uchiyama.

    TRDM is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    TRDM is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with TRDM.  If not, see <http://www.gnu.org/licenses/>.

@file   demoMarkerGeneration.cpp
@brief  sample c++ program to generate TRDM markers.
@author Yuji Oyamada
*/
#include "MarkerGenerator.h"

int main(int argc, char **argv)
{
    // set marker settings
    RandomDotsMarkerGenerator gen;
    gen.ShowAllVariables();

    // step 1 generate a single marker
    std::string basenameSingle = "../data/test_single";
    gen.MakeMarker(basenameSingle);
    int width;
    int height;
    Eigen::MatrixXd points;
    gen.LoadMarkerFromXml(basenameSingle+".xml", width, height, points);
    for(size_t i = 0; i < points.cols(); ++i){
        std::cout << "p[" << i << "] = (" << points.col(i).transpose() << ")" << std::endl;
    }

    size_t numMarkers = 5;
    gen.MakeMarkerSets("../data/markers", numMarkers);

    return 0;
}
